/**
 * Created by WarChief on 6/22/2015.
 */

var app = angular.module('myApp',['ui.router','myApp.controllers']);

app.config(
    function($stateProvider, $urlRouterProvider){
    $stateProvider
        .state('login',{
            url : '/',
            templateUrl : 'views/login.html',
            controller : 'loginCont'
        })
        .state('admin',{
            url : '/admin',
            templateUrl: 'views/admin.html',
            controller : 'adminCont'
        }).state('student',{
            url : '/student',
            templateUrl: 'views/student.html',
            controller : 'studentCont'
        }).state('allStudent',{
            url : '/allStudent',
            templateUrl: 'views/allStudents.html',
            controller : 'allStudentsCont'
        }).state('invalid',{
            url : '/error',
            templateUrl: 'views/invalidID.html',
            controller : ''
        });

        $urlRouterProvider.otherwise('/');
});

