/**
 * Created by WarChief on 6/22/2015.
 */
angular.module('myApp.controllers',[])

.controller('loginCont',
            function($scope, $state, $http, $window){
                $scope.txtUserID = '';
                $scope.txtUserPass = '';

                $scope.login = function (){

                    var data = {
                        id : $scope.txtUserID,
                        pass : $scope.txtUserPass
                    };

                    $http.post('/', data)
                        .success(function (data) {
                            console.log('data from Success ' + data);
                            if(data == 'admin')
                                $window.location = '#/admin';
                            else if (data == 'student')
                                $window.location = '#/student';
                            else
                                $window.location = '#/error';
                        })
                        .error(function (err){
                            console.log('data from Error ' + err)
                        });
                }
            })

.controller ('adminCont',
    function ($scope, $state , $http) {

        $scope.subjects = [];
//to add subjects
        $scope.addSubjects = function () {
            $scope.subjects.push({ name: $scope.txtSubName, id: $scope.txtSubId });
            $scope.txtSubName = null;
            $scope.txtSubId = null;
        };

// temp subject

        $scope.tempSubjects = [
            { name: $scope.txtSubName = "Maths", id: $scope.txtSubId = "M123" },
            { name: $scope.txtSubName = "Programming", id: $scope.txtSubId = "P123" },
            { name: $scope.txtSubName = "Islamiyat", id: $scope.txtSubId = "Is123" },
            { name: $scope.txtSubName = "Physics", id: $scope.txtSubId + "Ph123"},
            { name: $scope.txtSubName = "English", id: $scope.txtSubId = "En123"}
        ];
// temp subject control
        $scope.isChk = false;
        $scope.tempSubjectCall = function(index,isChecked){

            if(isChecked){
                $scope.subjects.push($scope.tempSubjects[index]);
            }else {
                $scope.subjects.splice($scope.subjects.indexOf($scope.tempSubjects[index]),1);
                console.log($scope.subjects.indexOf($scope.tempSubjects[index]));
            }

        };

//to add students
        $scope.addStudent = function () {
            var studentData = {
                id: $scope.txtStuId,
                pass: $scope.txtStuPass,
                admin: false,
                fname: $scope.txtStFName,
                lname: $scope.txtStuLName,
                tasks : [{ title: $scope.txtTitle,  description: $scope.txtDesc } ],
                class: $scope.txtStuClass,
                rating: $scope.txtRating,
                subjects: $scope.subjects
            };

            $http.post('/admin', studentData)
                .success(function (data) {
                    if(data)
                        alert('Student Added Successfully.');
                    else
                        alert('Student cannot be Added.');
                    $scope.txtStFName = null;
                    $scope.txtStuLName = null;
                    $scope.txtStuClass = null;
                    $scope.txtStuId = null;
                    $scope.txtStuPass = null;
                    $scope.txtTitle = null;
                    $scope.txtDesc = null;
                    $scope.txtRating = null;

                })
                .error(function (err){
                    console.log('data from Error ' + err)
                });
        }
// to Edit Students
        $scope.removeSubject = function(){
           var location =  $scope.$index;
            console.log($scope.location);
        }
    })


.controller('studentCont',
    function($scope){
        $scope.message = 'Hello Student';
    })

.controller('allStudentsCont',
    function($scope, $http){
        $scope.students = null;


        $http.get('/allStudents')
            .success(function(data){
            $scope.students = data;
          }).error(function (err) {
            alert(err);
        });


        $scope.message = 'Hello Student';
        $scope.toShow = null;
        $scope.edit = function (position){

           $scope.toShow = position;
        };
        $scope.tempSubjects = [
            { name:  "Maths", id: "M123" },
            { name:  "Programming", id:  "P123" },
            { name: "Islamiyat", id: "Is123" },
            { name:  "Physics", id:  "Ph123"},
            { name:  "English", id:  "En123"}
        ];
        $scope.tempSubjectsNames = [ "Maths","Programming", "Islamiyat", "Physics","English" ];
        // temp subject control
        //$scope.isChk = false;
        $scope.tempSubjectCall = function(index,isChecked,parentIndex){

            if(isChecked){
                $scope.students[parentIndex].subjects.push($scope.tempSubjects[index]);
            }else {
                $scope.students[parentIndex].subjects.splice($scope.getItsIndex($scope.tempSubjectsNames[index],$scope.students[parentIndex].subjects),1);
                //console.log($scope.getItsIndex($scope.tempSubjectsNames[index],$scope.students[parentIndex].subjects));
            }

        };

        $scope.isChecked = function(obj1,obj2){

            for(x in obj2){
                if(obj2[x].name == obj1){
                    return true;
                }
            }
            return false;
        };

        $scope.getItsIndex = function(obj1,obj2){

            for(x in obj2){
                if(obj2[x].name == obj1){
                    return x;
                }
            }
            return false;
        }

    });
