var express = require('express');
var router = express.Router();

//Student Class Class
var Class = function(student){
  var classStudent = 10;
  this.student = [];
  this.addStudent = function () {
    if(classStudent > 0){
    this.student.push(student);
      classStudent --;
      return "Student Added!"
    }else{
      return "Student cannot be Added!. Not enough seats."
    }

  }
};

//Task Class
var Task = function(title,description){
  this.title = title;
  this.description = description;
};

// Subjects Class
var Subjects = function(name, id){
  this.name = name;
  this.id = id;
};


var accounts = [{
  id : 'admin',
  pass : 'admin',
  admin : true,
  fname: 'Admin',
  lname: 'Admin'
},
  {
    id: 'aamir',
    pass: '123',
    admin: false,
    fname: 'Aamir',
    lname: 'Mughal',
    tasks : [{ title: 'Test Task',  description: 'This is the Description of the Task' } ],
    class: '3',
    rating: 4,
    subjects: [      {        name: 'Physics',        id: 'PHY123'      }    ]
  }

];

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/', function(req, res){
  var foundUser = false;
  for(x in accounts){
    if(req.body.id == accounts[x].id && req.body.pass == accounts[x].pass){
        if(accounts[x].admin){
          res.send('admin');
          foundUser = true;
        }else if(!accounts[x].admin){
          res.send('student');
          foundUser = true;
        }
    }
  }
  if (!foundUser){
    res.send('Invalid Id and Pass');
  }
});

router.post('/admin',function(req , res){

  if(checkAvailableClass(req.body.class)){
  accounts.push(req.body);
  res.send(true);

  } else {
    res.send(false);
    console.log(accounts.length);
  }
});

router.get('/allStudents', function (req, res) {
  res.send(accounts);
});

//to check Available seats into class and assign a seat into class
var checkAvailableClass = function (className) {
  var count = 0;
  for(x in accounts){
    if(accounts[x].class == className){
      count++;
    }
  }
  if(count < 10){
    return true;
  }else{
    return false;
  }
};

module.exports = router;
